## Swordfish TFA

TFA is a base module for providing two-factor authentication for your Drupal
site. As a base module, TFA handles the Drupal integration work,
providing flexible and well tested interfaces to enable seamless, and
configurable, choice of various two-factor authentication solutions like
Time-based One Time Passwords, SMS-delivered codes, recovery codes, or
integrations with third-party suppliers like Authy, Duo and others.

This module is a bundle of all the TFA dependencies. 

### Installation

Install modules with composer:
$ `composer require drupal/tfa drupal/ga_login drupal/real_aes`

Enable a module and its dependencies with drush:
$ `drush en swordfish_tfa -y`

### Getting started

This module requires a few dependencies to be setup before it can be configured.

#### Settings

The key module provides Drupal access to an encryption key you create. Setting
up the key module:

* Check key in `/keys/encrypt.key`
* Visit the Keys module's configuration page `/admin/config/system/keys` and "Add Key"
  * Name your Key
  * Key type: "Encryption"
  * Key size: "256"
  * Provider: "File"
  * File location: `../keys/encrypt.key`
  * Check on: "Base64-encoded"
  * Save

The encrypt module allows the site owner to define encryption profiles that
can be reused throughout Drupal. The TFA module requires an encryption profile
to be defined to be configured properly.

The encryption service is configured through the settings form, found at
`/admin/config/system/encryption`.
* Visit the Encrypt module's configuration page and "Add Encryption Profile"
  * Label your Encryption Profile
  * Encryption method: "Authenticated AES (Real AES)" - or the encryption
    method of your choice.
  * Encryption Key: Select the Key you created in the previous step.
  * Save

##### TFA Configuration

Now you should be ready to configure the TFA module.
* Visit the TFA module's configuration page `/admin/config/people/tfa`.
  * Enable TFA
  * Select "Roles required to set up TFA" to Authenticated user.
  * Select "Allowed Validation plugins" to GA Login Time-based OTP(TOTP).
  * Select "Default Validation plugin" to GA Login Time-based OTP(TOTP).
  * Encryption Profile: Select the Encryption Profile you created in the
    previous step.
  * Adjust other settings as desired.
  * Save
* Grant "Set up TFA for account" to "Authenticated user"
  * Consider granting "Require TFA process" for some roles
* Visit your account's Security tab: `user/[uid]/security/tfa`
  * Configure the selected Validation Plugins as desired for your account.

##### How to use

* Visit your account's Security tab: `user/[uid]/security/tfa`
  * Go to link "Set up application"
  * Confirm your password
* Install authentication code application on your mobile or desktop device:
  * Google Authenticator (Android/iPhone/BlackBerry)
  * Authy (Android/iPhone)
  * FreeOTP (Android)
  * GAuth Authenticator (desktop)
* Scan QR code
* Insert code from app to "Application verification code" and Verify.

##### TFA, Testing, and Development

It can be hard to test user authentication in automated tests with the TFA
module enabled. Development environments also will likely struggle to login
unless they disable TFA or reset the secrets for an account. One solution is
to disable the module in the development and testing environment. To quickly
disable the module you can run these drush commands to set some config:

* Disable TFA with `drush config-set tfa.settings enabled 0`
* Enable TFA with `drush config-set tfa.settings enabled 1`

